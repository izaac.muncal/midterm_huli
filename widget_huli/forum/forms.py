from django import forms
from .models import ForumPost

class ForumPostForm(forms.ModelForm):
    pub_datetime = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'],
                                       widget = forms.DateTimeInput(format = '%d/%m/%Y %H:%M',
                                                                    attrs = {'class': 'form-control'}),
                                       label = 'Date and Time')
    class Meta:
        model = ForumPost
        fields = '__all__'
    