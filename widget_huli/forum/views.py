from django.shortcuts import render
from django.urls import reverse
from django.views.generic import DetailView, CreateView, UpdateView

from .models import ForumPost, Reply
from .forms import ForumPostForm

# Create your views here.

def ForumViews(request):
    forum = ForumPost.objects.all()
    context = {'forum':forum}
    return render(request, 'forum/forum.html', context)

class ForumPostDetails(DetailView):
    template_name= "forum/forumpost-details.html"
    model = ForumPost
                                    
class ForumPostNew(CreateView):
    form_class = ForumPostForm
    template_name = "forum/forumpost-add.html"
    
    def get_success(self):
        return reverse('forum:forumpostnew', kwargs = {'pk': self.object.id}, 
                    current_app=self.request.resolver_match.namespace)
    
class ForumPostEdit(UpdateView):
    form_class = ForumPostForm
    template_name = "forum/forumpost-edit.html"
    queryset = ForumPost.objects.all()
    
    def get_success_url(self):
        return reverse('forum:forumpostdetails', kwargs = {'pk': self.object.pk}, 
                       current_app=self.request.resolver_match.namespace)