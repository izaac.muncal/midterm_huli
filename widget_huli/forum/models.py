from django.db import models
from django.urls import reverse
from dashboard.models import WidgetUser

# Create your models here.
class ForumPost(models.Model):
    title = models.CharField(max_length=50)
    body = models.TextField()
    author = models.ForeignKey(
        WidgetUser,
        on_delete=models.CASCADE
    )
    pub_datetime = models.DateTimeField()
    
    class Meta:
        ordering = ['-pub_datetime']
    
    def __str__(self):
        return '{}'.format(self.title)
    
    def get_absolute_url(self):
        return reverse("forum:forumpostdetails", kwargs={"pk": self.pk})

class Reply(models.Model):
    body = models.TextField()
    author = models.CharField(max_length=50)
    pub_datetime = models.DateTimeField()
    forum_post = models.ForeignKey(
        ForumPost,
        on_delete=models.CASCADE
    )
    def get_absolute_url(self):
        return reverse("forum:replydetails", kwargs={'pk': self.pk})