from django.db import models
from django.urls import reverse


class Department(models.Model):
    dept_name = models.CharField(max_length=60)
    home_unit = models.CharField(max_length=100)

    def __str__(self):
        return '{}, {}'.format(self.dept_name, self.home_unit)


class WidgetUser(models.Model):
    first_name = models.CharField(max_length=20)
    middle_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    department = models.ForeignKey(Department, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return '{}, {}'.format(self.last_name, self.first_name)

    def get_absolute_url(self):
        return reverse('dashboard:widgetuser-details', kwargs={'pk': self.pk})

    def get_update_url(self):
        return reverse('dashboard:widgetuser-edit', kwargs={'pk': self.pk})

